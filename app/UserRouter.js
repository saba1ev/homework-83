const express = require('express');
const User = require('../modules/UserModule');

const router = express.Router();

router.post('/', async(req, res)=>{
  const user = new User(req.body);

  try {
    await user.save();
    return res.send(user)

  } catch (e) {
    return res.status(400).send(e)
  }
});

router.post('/sessions', async (req, res) => {
  const user = await User.findOne({username: req.body.username});

  if (!user) {
    return res.status(400).send({error: 'Username does not exist'});
  }

  const isMatch = await user.checkPassword(req.body.password);

  if (!isMatch) {
    return res.status(400).send({error: 'Password is wrong'});
  }
  user.generateToken();
  await user.save();
  res.send({token: user.token});
});

module.exports = router;