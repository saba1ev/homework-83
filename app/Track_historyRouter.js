const express = require('express');
const Track_History = require('../modules/Track_historyModule');
const User = require('../modules/UserModule');

const router = express.Router();

router.post('/', async (req, res)=>{
  const token = req.get('Token');
  if (!token){
    res.status(401).send({error: "Unauthorized"})
  }
  const user = await User.findOne({token});
  if (!user){
    res.status(401).send({error: 'Unauthorized'})
  }
  const datetime = new Date().toISOString();
  const track = ({user: user._id, track: req.body.track, datetime});
  const TrackHistory = new Track_History(track);
  await TrackHistory.save();
  res.send(TrackHistory);

});

module.exports = router;