const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const config = require('./config');

const Artist = require('./app/ArtistRouter');
const Album = require('./app/AlbumRouter');
const Track = require('./app/TrackRouter');
const User = require('./app/UserRouter');
const Track_history = require('./app/Track_historyRouter');



const app = express();
app.use(cors());
app.use(express.json());

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(()=>{
  app.use('/artist', Artist());
  app.use('/album', Album());
  app.use('/track', Track());
  app.use('/user', User);
  app.use('/track_history', Track_history);

  app.listen(port, ()=>{
    console.log(`We are started on ${port} port`)
  })
});