const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const nanoid = require('nanoid');

const SALT_WORK_FACTOR = 10;


const UserSchema = mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  token: {
    type: String
  }

});

UserSchema.methods.checkPassword = function(pass){
  return bcrypt.compare(pass, this.password)
};
UserSchema.methods.generateToken = function(){
  this.token = nanoid()
}

UserSchema.pre('save', async function (next) {
  const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
  const hash = await bcrypt.hash(this.password, salt);
  this.password = hash;
  next();
});
UserSchema.set('toJSON', {
  transform: (doc, ret, option) =>{
    delete ret.password;
    return ret
  }
});
const UserModule = mongoose.model('User', UserSchema);

module.exports = UserModule;